package sample.Model;

import sample.Archivos.ArchivoIndice;
import sample.Archivos.ArchivoMaestro;

import javax.swing.*;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashMap;

public class Grafo {
    private int numeroNodos;
    private HashMap<Character, float[][]> matricesAdyacencia;

    public Grafo(){
        numeroNodos=0;
        matricesAdyacencia = new HashMap<Character, float[][]>();
    }

    public void inicializar(int _numNodos){
        numeroNodos = _numNodos;
        matricesAdyacencia = new HashMap<Character, float[][]>();
    }

    public void agregarPropiedad(char llave){
        if(numeroNodos>0){
            float[][] matrizInicial = new float[numeroNodos][numeroNodos];
            for (int i = 0; i < numeroNodos; i++) {
                for (int j = 0; j < numeroNodos; j++) {
                    matrizInicial[i][j]=0;
                }
            }
            matricesAdyacencia.putIfAbsent(llave,matrizInicial);
        }else{
            JOptionPane.showMessageDialog(null,"El grafo no esta inicializado");
        }
    }

    public void asignarMatriz(char llave, float[][] matriz){
        if(numeroNodos>0){
            matricesAdyacencia.replace(llave,matriz);
        }else{
            JOptionPane.showMessageDialog(null,"El grafo no esta inicializado");
        }
    }

    public float[][] obtenerMatriz(char llave){
        if (numeroNodos>0){
            return matricesAdyacencia.get(llave);
        }else{
            JOptionPane.showMessageDialog(null,"El grafo no esta inicializado");
            return null;
        }
    }

    public void guardarEnArchivos(String nombre){
        if (numeroNodos>0&&!matricesAdyacencia.isEmpty()){
            String nombreMaestro = nombre+"Maestro.gr";
            String nombreIndice = nombre+"Indice.gr";

            Object[] llaves = matricesAdyacencia.keySet().toArray();

            char[] llavesC = new char[llaves.length];

            for (int i = 0; i < llaves.length; i++) {
                llavesC[i] = (char)llaves[i];
            }

            //System.out.println(Arrays.toString(llavesC));
            Arrays.sort(llavesC);

            ArchivoIndice archInd = new ArchivoIndice();
            ArchivoMaestro archMt = new ArchivoMaestro();

            archInd.inicializar(nombreIndice);
            archMt.inicializar(nombreMaestro);

            for (int i = 0; i < llavesC.length; i++) {
                float[][] registro = matricesAdyacencia.get(llavesC[i]);
                try {
                    archInd.escribir(llavesC[i],i+1);
                    archMt.escribir(llavesC[i],registro);
                }catch (Exception e){
                    JOptionPane.showMessageDialog(null,"Fallo al guardar los datos");
                }
            }

            try{
                archInd.archivo.close();
                archMt.archivo.close();
            }catch (Exception e){
                JOptionPane.showMessageDialog(null,"error al cerrar archivos");

            }

            JOptionPane.showMessageDialog(null,"Grafo guardado con éxito!");


        }else{
            JOptionPane.showMessageDialog(null,"Para guardar primero crea un grafo con al menos una propiedad");
        }

    }

    public void limpiar(){
        numeroNodos=0;
        matricesAdyacencia.clear();
    }

    public HashMap<Character, float[][]> getMatricesAdyacencia() {
        return matricesAdyacencia;
    }

    public int getNumeroNodos() {
        return numeroNodos;
    }
}
