package sample.Archivos;

import javax.swing.*;
import java.io.IOException;
import java.io.RandomAccessFile;

public class ArchivoMaestro {
    public RandomAccessFile archivo;

    public void inicializar(String nombre){
        try{
            archivo = new RandomAccessFile(nombre,"rw");
        }catch (Exception e){
            JOptionPane.showMessageDialog(null,"fallo en creacion de archivo "+nombre);
        }
    }

    public void escribir(char llave, float[][] registro) throws IOException {
        archivo.writeChar(llave);
        for (int i = 0; i < registro.length; i++) {
            for (int j = 0; j < registro[i].length; j++) {
                archivo.writeFloat(registro[i][j]);
            }
        }
    }
}
