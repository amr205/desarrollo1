package sample.Archivos;

import javax.swing.*;
import java.io.IOException;
import java.io.RandomAccessFile;

public class ArchivoIndice {
    public RandomAccessFile archivo;

    public void inicializar(String nombre){
        try{
            archivo = new RandomAccessFile(nombre,"rw");
        }catch (Exception e){
            JOptionPane.showMessageDialog(null,"fallo en creacion de archivo "+nombre);
        }
    }

    public void escribir(char llave, int direccionLogica) throws IOException {
        archivo.writeChar(llave);
        archivo.write(direccionLogica);
    }

}
