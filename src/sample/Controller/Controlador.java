package sample.Controller;

import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import sample.Model.Grafo;

import javax.swing.*;
import java.util.Arrays;
import java.util.List;

public class Controlador {
    Grafo grafo = new Grafo();
    char propiedadMostrada = ' ';
    @FXML
    Button editarPropiedadBtn;
    @FXML
    Button crearPropiedadBtn;
    @FXML
    Button guardarMatrizBtn;
    @FXML
    TableView<List<String>> tablaMatriz;
    @FXML
    Button guardarBtn;
    @FXML
    Button crearNuevoBtn;

    public void accionCrearNuevo(MouseEvent mouseEvent) {
        try {
            int num = Integer.parseInt(JOptionPane.showInputDialog("Ingresa el número de nodos del grafo"));
            if(num<=0){
                JOptionPane.showMessageDialog(null,"El valor tiene que ser un entero positivo");
            }else{
                grafo.limpiar();
                grafo.inicializar(num);
            }
        }catch (Exception e){
            JOptionPane.showMessageDialog(null,"El valor tiene que ser un entero positivo");
            e.printStackTrace();
        }

    }

    public void accionGuardar(MouseEvent mouseEvent) {
        String nombre = JOptionPane.showInputDialog("Ingrese el nombre del archivo");
        grafo.guardarEnArchivos(nombre);
    }

    public void accionEditarPropiedad(MouseEvent mouseEvent) {
        String input = JOptionPane.showInputDialog("Ingrese el nombre de la propiedad (caracter)");
        if (input.length()>1){
            JOptionPane.showMessageDialog(null,"Inserte solo un caracter");
        }else{
           char llave = input.charAt(0);
           actualizarTablaMatriz(llave);
        }
    }

    public void accionCrearPropiedad(MouseEvent mouseEvent) {
        String input = JOptionPane.showInputDialog("Ingrese el nombre de la propiedad (caracter)");
        if (input.length()>1){
            JOptionPane.showMessageDialog(null,"Inserte solo un caracter");
        }else{
            char llave = input.charAt(0);
            grafo.agregarPropiedad(llave);
            actualizarTablaMatriz(llave);
        }
    }

    public void accionGuardarMatriz(MouseEvent mouseEvent) {
        int numNodos = grafo.getNumeroNodos();
        if(numNodos>0) {
            boolean puedeGuardar = grafo.getMatricesAdyacencia().containsKey(propiedadMostrada);
            if (puedeGuardar) {
                float[][] mat = new float[numNodos][numNodos];
                try {
                    for (int i = 0; i < numNodos; i++) {
                        for (int j = 1; j < numNodos + 1; j++) {
                            String dato = tablaMatriz.getItems().get(i).get(j);
                            mat[i][j - 1] = Float.parseFloat(dato);
                        }
                    }

                    grafo.asignarMatriz(propiedadMostrada,mat);
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null,"Los datos en la tabla deben ser numéricos");
                }
            }
        }
    }

    private void actualizarTablaMatriz(char llave){
        propiedadMostrada = llave;
        float[][] matriz = grafo.obtenerMatriz(llave);

        if(matriz!=null) {

            //REINICIAR TABLA
            tablaMatriz.getItems().clear();
            tablaMatriz.getColumns().clear();

            //PERMITIR QUE LA TABLA SEA EDITABLE
            tablaMatriz.getSelectionModel().setCellSelectionEnabled(true);
            int numNodos =matriz.length;

            //AGREGAR COLUMNAS
            for (int i = 0; i < numNodos + 1; i++) {
                TableColumn<List<String>, String> col;
                if (i == 0) {
                    col = new TableColumn<>("  ");
                    col.setEditable(false);
                } else
                    col = new TableColumn<>("Nodo " + (i));

                col.setOnEditCommit(event -> {
                    List<String> lista = event.getRowValue();
                    TablePosition<?, ?> position = event.getTablePosition();
                    int column = position.getColumn();
                    lista.set(column, event.getNewValue());
                });
                col.setMinWidth(80);
                col.setCellFactory(TextFieldTableCell.forTableColumn());
                final int colIndex = i;
                col.setCellValueFactory(data -> {
                    List<String> rowValues = data.getValue();
                    String cellValue;
                    if (colIndex < rowValues.size()) {
                        cellValue = rowValues.get(colIndex);
                    } else {
                        cellValue = "0";
                    }
                    return new SimpleStringProperty(cellValue);
                });
                tablaMatriz.getColumns().add(col);
            }

            //AÑADIR INFORMACION
            for (int j = 0; j < numNodos; j++) {
                String[] data = new String[numNodos + 1];
                for (int i = 0; i < numNodos + 1; i++) {
                    if (i == 0)
                        data[i] = "Nodo " + (j+1);
                    else
                        data[i] = ""+matriz[j][i-1];
                }
                tablaMatriz.getItems().add(Arrays.asList(data));
            }
            tablaMatriz.setEditable(true);

        }else{
            JOptionPane.showMessageDialog(null,"No existe la propiedad: "+llave);
        }
    }
}
